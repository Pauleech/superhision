import os
import cv2
import logging
import platform
import multiprocessing
from modules.logering import setup_logger

# number of cores
cores = multiprocessing.cpu_count()

# operating system
system = platform.system()

# root data_path
root_path = os.path.dirname(os.path.realpath(__file__))
data_folder = os.path.join(root_path, "data")

# folders
patterns_folder = os.path.join(data_folder, "patterns")
videos_folder = os.path.join(data_folder, "videos")
renaming_folder = os.path.join(data_folder, "renaming")
params_folder = os.path.join(root_path, "src/detection/params")

# seed
seed = 42

# logging
log_level = logging.INFO
log_name = os.path.join(root_path, "superhision.log")
logger = setup_logger("logger", log_name, log_level)

# clahe parameters
clahe_clip_limit = 2.
clahe_tile_grid_size = (8, 8)
clahe_params = {"clipLimit": clahe_clip_limit, "tileGridSize": clahe_tile_grid_size}

# detector parameters
detector_image_scale = 0.2
detector_pattern_scale = 0.5
detector_use_clahe = True
detector_use_blur = True
detector_lowe_ratio = 0.6
detector_close_share = 0.9
detector_validity_share = 0.55
detector_validity_min = 10
detector_min_matches = 10

# orb parameters
orb_edge_threshold = 21
orb_nfeatures = 500
orb_score_type = cv2.ORB_FAST_SCORE
orb_scale_factor = 1.2
orb_params = {"nfeatures": orb_nfeatures, "scaleFactor": orb_scale_factor,
              "edgeThreshold": orb_edge_threshold, "scoreType": orb_score_type}

# flann parameters
flann_index_lsh = 6
flann_table_number = 6
flann_key_size = 12
flann_probe = 1
flann_search_params = dict(checks=50)
flann_index_params = {"algorithm": flann_index_lsh, "table_number": flann_table_number,
                      "key_size": flann_key_size, "multi_probe_level": flann_probe}

# detector params
use_clahe = True
use_blur = True
detector_params = {"clahe_params": clahe_params, "orb_params": orb_params,
                   "flann_index_params": flann_index_params, "flann_search_params": flann_search_params,
                   "image_scale": detector_image_scale, "pattern_scale": detector_pattern_scale,
                   "use_clahe": detector_use_clahe, "use_blur": detector_use_blur,
                   "lowe_ratio": detector_lowe_ratio, "close_share": detector_close_share,
                   "min_matches": detector_min_matches, "validity_share": detector_validity_share,
                   "validity_min": detector_validity_min}

# testing
undetected = "Unknown"
