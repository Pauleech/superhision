def split_list(values, num_parts):
    # type: (list, int) -> list
    """
    Equally splits list on parts.

    :rtype: list.
    :param values: list.
    :param num_parts: number of parts.
    :return: splitted list.
    """
    avg = len(values) / float(num_parts)
    output = []
    last = 0.0
    while last < len(values):
        output.append(values[int(last):int(last + avg)])
        last += avg
    return output


def flatten(values):
    # type: (list) -> list
    """
    Returns flatten list.

    :rtype: list
    :param values: list.
    :return: flatten list.
    """
    return [item for sublist in values for item in sublist]
