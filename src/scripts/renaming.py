import os
import config
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_patterns_dict():
    # type: () -> dict
    """
    Returns patterns dictionary: {label: pattern}.

    :rtype: dict
    :return: patterns dictionary.
    """
    output = dict()
    for label, filename in enumerate(os.listdir(config.patterns_folder)):
        if "_" in filename:
            pattern = filename[:filename.find("_")]
            output[label] = pattern
    return output


def rename(renaming_folder, exemplars, offset):
    # type: (str, int, int) -> None
    """
    Renames images in a given folder corresponding to the patterns names.

    :rtype: None
    :param renaming_folder: folder with images to rename.
    :param exemplars: number of exemplars for each pattern.
    :param offset: offset for renaming number.
    :return: None.
    """
    patterns = get_patterns_dict()
    photo_index = 1
    pattern_index = 0
    for filename in os.listdir(renaming_folder):
        if filename.endswith(".mov"):
            old_name = os.path.join(renaming_folder, filename)
            filename = "{}_video{:02d}.mov".format(patterns[pattern_index], photo_index + offset)
            new_name = os.path.join(renaming_folder, filename)
            print "{} => {}".format(old_name, new_name)
            if old_name != new_name:
                os.rename(old_name, new_name)
            photo_index += 1
            if photo_index > exemplars:
                photo_index = 1
                pattern_index += 1


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    rename(config.renaming_folder, exemplars=5, offset=0)


if __name__ == "__main__":
    main()
