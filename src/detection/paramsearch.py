import os
import cv2
import config
import numpy as np
import pandas as pd
from sklearn import metrics
from collections import OrderedDict
from detector import PatternsDetector
from modules.dateutils import get_datestr
from multiprocessing.dummy import Pool
from modules.logering import setup_logger
from modules.parallel import split_list, flatten
from modules.timing import elapsed_timer, timeit
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_patterns_dict():
    # type: () -> dict
    """
    Returns patterns dictionary: {pattern: label}.

    :rtype: dict
    :return: patterns dictionary.
    """
    output = OrderedDict()
    output[config.undetected] = -1
    for label, filename in enumerate(os.listdir(config.patterns_folder)):
        if "_" in filename:
            pattern = filename[:filename.find("_")]
            output[pattern] = label
    return output


def get_true_labels(filenames):
    # type: (list) -> list
    """
    Returns true labels list for a given list of filenames.

    :rtype: list
    :param filenames: videos filenames.
    :return: labels list.
    """
    patterns_dict = get_patterns_dict()
    files_patterns = map(lambda name: name[:name.rfind("_")], filenames)
    return map(lambda pattern: patterns_dict[pattern], files_patterns)


def get_metrics(y_true, y_pred, labels):
    # type: (list, list, list) -> (ndarray, float)
    """
    Returns class accuracies and overall accuracy score for given true and predicted labels.

    :rtype: (ndarray, float)
    :param y_true: true labels.
    :param y_pred: predicted labels.
    :param labels: list of labels.
    :return: class accuracies and overall accuracy score.
    """
    cmat = metrics.confusion_matrix(y_true, y_pred, labels=labels[1:])
    accuracies = ["-"] + list(1. * cmat.diagonal() / cmat.sum(axis=1))
    return accuracies, metrics.accuracy_score(y_true, y_pred)


@timeit(logger, "testing patterns detector", title=True)
def test_detector(model, filenames, y_true):
    # type: (object, list, list) -> (ndarray, float, float)
    """
    Tests patterns detector.
    Returns class accuracies, overall accuracy and average prediction time for given true and predicted labels.

    :rtype: (ndarray, float, float)
    :param model: patterns detector model.
    :param filenames: videos filenames.
    :param y_true: true labels.
    :return: class accuracies, overall accuracy score and average time for prediction.
    """

    def predict_labels(_filenames):
        # type: (list) -> (list, list)
        """
        Predicts labels for a given list of video filenames.

        :rtype: (list, list)
        :param _filenames: videos filenames.
        :return: predicted labels.
        """
        predicted = []
        elapsed_list = []
        for filename in _filenames:
            video = cv2.VideoCapture(os.path.join(config.videos_folder, filename))
            with elapsed_timer(logger, "predicting label", verbose=False) as elapsed:
                label = model.predict_video(video)
                elapsed_list.append(elapsed())
            predicted.append(label)
        return predicted, elapsed_list

    patterns_dict = get_patterns_dict()
    filenames = split_list(filenames, config.cores)

    pool = Pool(config.cores)
    pool_results = pool.imap(predict_labels, filenames)
    pool.close()
    pool.join()
    y_pred, times = zip(*pool_results)
    y_pred, times = flatten(y_pred), flatten(times)
    average_time = np.array(times).mean()

    labels = sorted(patterns_dict.values())
    accuracies, accuracy_score = get_metrics(y_true, y_pred, labels)
    return accuracies, accuracy_score, average_time


@timeit(logger, "searching params patterns detector", title=True)
def search_params(filenames, y_true, params):
    # type: (list, list, list) -> None
    """
    Tests patterns detector in grid search manner and saves results to .csv file.

    :rtype: None
    :param filenames: videos filenames.
    :param y_true: true labels.
    :param params: list of grid search parameters.
    :return: None.
    """
    df = pd.DataFrame()
    patterns_dict = get_patterns_dict()
    patterns = [config.undetected] + [key for key, value in patterns_dict.iteritems() if value in y_true]
    for i, param_grid in enumerate(params):
        with elapsed_timer(logger, "detector params {:02d}/{:02d}".format(i + 1, len(params)), title=True):
            detector = PatternsDetector(**param_grid)
            detector.fit(config.patterns_folder)
            accuracies, accuracy_score, average_time = test_detector(detector, filenames, y_true)
            row = {key: value for (key, value) in zip(patterns, accuracies)}
            row["params"] = str(param_grid)
            row["accuracy"] = accuracy_score
            row["avg_time"] = average_time
            df = df.append(pd.DataFrame(row, index=[i]))
    df.to_csv(os.path.join(config.params_folder, "{}.csv").format(get_datestr()), index=False, float_format="%.2f")


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    filenames = [filename for filename in os.listdir(config.videos_folder) if "_" in filename]
    y_true = get_true_labels(filenames)

    grid_search_params = list()
    grid_search_params.append({
        "clahe_params": config.clahe_params,
        "orb_params": config.orb_params,
        "flann_index_params": config.flann_index_params,
        "flann_search_params": config.flann_search_params,
        "image_scale": config.detector_image_scale,
        "pattern_scale": config.detector_pattern_scale,
        "use_clahe": config.detector_use_clahe,
        "use_blur": config.detector_use_blur,
        "lowe_ratio": config.detector_lowe_ratio,
        "close_share": config.detector_close_share,
        "min_matches": config.detector_min_matches,
        "validity_share": config.detector_validity_share,
        "validity_min": config.detector_validity_min
    })
    grid_search_params.append({
        "clahe_params": config.clahe_params,
        "orb_params": config.orb_params,
        "flann_index_params": config.flann_index_params,
        "flann_search_params": config.flann_search_params,
        "image_scale": config.detector_image_scale,
        "pattern_scale": config.detector_pattern_scale,
        "use_clahe": config.detector_use_clahe,
        "use_blur": config.detector_use_blur,
        "lowe_ratio": config.detector_lowe_ratio,
        "close_share": config.detector_close_share,
        "min_matches": config.detector_min_matches,
        "validity_share": config.detector_validity_share,
        "validity_min": 20
    })
    search_params(filenames, y_true, grid_search_params)


if __name__ == "__main__":
    main()
