import os
import cv2
import config
import operator
import numpy as np
import cPickle as pickle
from collections import Counter
from modules.timing import timeit
from modules.dateutils import get_datestr
from modules.logering import setup_logger
logger = setup_logger(__name__, config.log_name, config.log_level)


class PatternsDetector:
    def __init__(self, clahe_params, orb_params, flann_index_params, flann_search_params, name=None,
                 image_scale=1., pattern_scale=0.5, use_clahe=True, use_blur=True,
                 lowe_ratio=0.6, close_share=0.9, min_matches=0, validity_share=0.55, validity_min=20):
        # type: (dict, dict, dict, dict, str, float, float, bool, bool, float, float, int, float, int) -> None
        """
        Initiates PatternsDetector object.

        :rtype: None
        :param clahe_params: clahe model parameters dictionary.
        :param orb_params: ORB detector parameters dictionary.
        :param flann_index_params: FLANN matcher index parameters dictionary.
        :param flann_search_params: FLANN matcher search parameters dictionary.
        :param name: model name.
        :param image_scale: image/frame scaling factor.
        :param pattern_scale: pattern scaling factor.
        :param use_clahe: use clahe on frames and patterns.
        :param use_blur: use Gaussian blur on frames.
        :param lowe_ratio: lowe ratio matching parameter.
        :param close_share: share factor of two best matches numbers.
        :param min_matches: number of matches to make prediction.
        :param validity_share: share factor of valid pattern in a frames validation list.
        :param validity_min: number of last frames accounting in validation process.
        :return: None.
        """
        self.name = get_datestr() if name is None else name
        self.image_scale = image_scale
        self.pattern_scale = pattern_scale
        self.use_clahe = use_clahe
        self.use_blur = use_blur
        self.lowe_ratio = lowe_ratio
        self.close_share = close_share
        self.validity_share = validity_share
        self.validity_min = validity_min
        self.min_matches = min_matches

        self.clahe = cv2.createCLAHE(**clahe_params)
        self.orb_detector = cv2.ORB_create(**orb_params)
        self.matcher = cv2.FlannBasedMatcher(flann_index_params, flann_search_params)

        self.patterns = dict()
        self.labels = {-1: config.undetected}

    @timeit(logger, "computing patterns descriptors and keypoints", title=True)
    def fit(self, patterns_folder):
        # type: (str) -> None
        """
        Computes patterns descriptors and keypoints for a given folder with pattern images.

        :rtype: None
        :param patterns_folder: folder with training images.
        :return: None.
        """
        for label, filename in enumerate(os.listdir(patterns_folder)):
            if "_" in filename:
                pattern = filename[:filename.find("_")]  # example: Butterfly_01.jpg
                pattern_img = cv2.imread(os.path.join(patterns_folder, filename), 0)
                resized_img = cv2.resize(pattern_img, (0, 0), fx=self.pattern_scale, fy=self.pattern_scale)
                if self.use_clahe:
                    resized_img = self.clahe.apply(resized_img)
                pattern_keypoints = self.orb_detector.detect(resized_img, None)
                pattern_descriptors = self.orb_detector.compute(resized_img, pattern_keypoints, None)[1]
                if pattern in self.patterns:
                    self.patterns[pattern]["kp"] += pattern_keypoints
                    self.patterns[pattern]["des"] = \
                        np.concatenate((self.patterns[pattern]["des"], pattern_descriptors), axis=0)
                else:
                    self.patterns[pattern] = dict()
                    self.patterns[pattern]["img"] = pattern_img
                    self.patterns[pattern]["keypoints"] = pattern_keypoints
                    self.patterns[pattern]["descriptors"] = pattern_descriptors
                    self.patterns[pattern]["label"] = label
                    self.labels[label] = pattern

    def preprocess_image(self, image):
        # type: (ndarray) -> ndarray
        """
        Returns preprocessed image.

        :rtype: image
        :param image: an image.
        :return: preprocessed image.
        """
        image = cv2.resize(image, (0, 0), fx=self.image_scale, fy=self.image_scale)
        if self.use_blur:
            image = cv2.GaussianBlur(image, (1, 1), 0)
        if self.use_clahe:
            image = self.clahe.apply(image)
        return image

    def get_matches(self, image):
        # type: (ndarray) -> dict
        """
        Returns dictionary of patterns matches for a given image.

        :rtype: dict
        :param image: an image.
        :return: dictionary of patterns matches.
        """
        output = dict()
        image_keypoints = self.orb_detector.detect(image, None)
        image_descriptors = self.orb_detector.compute(image, image_keypoints, None)[1]
        for pattern in self.patterns.keys():
            output[pattern] = 0
            pattern_descriptors = self.patterns[pattern]["descriptors"]
            matches = self.matcher.knnMatch(pattern_descriptors, image_descriptors, k=2)
            for match in matches:
                if len(match) != 2:
                    continue
                (m, n) = match
                if m.distance < self.lowe_ratio * n.distance:
                    output[pattern] += 1
        return output

    def predict_label(self, image):
        # type: (ndarray) -> int
        """
        Returns a label (pattern) for a given image (without preprocessing).

        :rtype: int
        :param image: an image.
        :return: a label (pattern).
        """
        output = -1
        matches = self.get_matches(image)
        sorted_matches = sorted(matches.iteritems(), key=operator.itemgetter(1), reverse=True)
        first_best, second_best = sorted_matches[:2]
        is_confident = first_best[1] * self.close_share > second_best[1]
        is_detected = first_best[1] > self.min_matches
        if is_confident and is_detected:
            output = self.patterns[first_best[0]]["label"]
        return output

    def predict_image(self, image):
        # type: (ndarray) -> int
        """
        Predicts a label (pattern) for a given image.

        :rtype: int
        :param image: an image.
        :return: a label (pattern).
        """
        image = self.preprocess_image(image)
        return self.predict_label(image)

    def iter_frames(self, video):
        # type: (video) -> ndarray
        """
        Iterates over a video frames for a given video capture.

        :rtype: ndarray
        :param video: video capture.
        :return: yields video frame.
        """
        pos_frame = video.get(1)
        while True:
            flag, frame = video.read()
            if flag:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                frame = self.preprocess_image(frame)
                yield frame
                pos_frame = video.get(1)
            else:
                video.set(1, pos_frame - 1)
                logger.warn("frame {} is not ready".format(int(pos_frame)))
                cv2.waitKey(1000)
            if video.get(1) == video.get(cv2.CAP_PROP_FRAME_COUNT):
                break
        video.release()
        cv2.destroyAllWindows()

    def predict_video(self, video):
        # type: (video) -> int
        """
        Predicts a label (pattern) for a given video capture.

        :rtype: int
        :param video: video capture.
        :return: a label (pattern).
        """
        output = -1
        frames_labels = []
        for i, frame in enumerate(self.iter_frames(video)):
            try:
                frame_label = self.predict_label(frame)
                frames_labels.append(frame_label)
                logger.debug("..frame #{} is {}".format(i, self.labels[frame_label]))
                if len(frames_labels) >= self.validity_min:
                    most_predicted = Counter(frames_labels[-self.validity_min:]).most_common(1)[0]
                    is_confident = most_predicted[1] > self.validity_min * self.validity_share
                    is_detected = most_predicted[0] != -1
                    if is_detected and is_confident:
                        output = most_predicted[0]
                        break
            except cv2.error as e:
                logger.error("error occurred: {}".format(e))
        return output

    @timeit(logger, "saving model", title=True)
    def save(self, path):
        # type: (str) -> None
        """
        Saves object in a pickle file.

        :rtype: None
        :param path: path to save.
        :return: None.
        """
        with open(os.path.join(path, "{}.pickle".format(self.name)), "wb") as handle:
            pickle.dump(self, handle, protocol=pickle.HIGHEST_PROTOCOL)

    @staticmethod
    @timeit(logger, "loading model", title=True)
    def load(filename):
        # type: (str) -> PatternsDetector
        """
        Loads object from a pickle file.

        :rtype: PatternsDetector
        :param filename: filename to load.
        :return: class object.
        """
        with open(filename, "rb") as handle:
            return pickle.load(handle)
