import os
import cv2
import config
from sklearn import metrics
from detector import PatternsDetector
from multiprocessing.dummy import Pool
from modules.logering import setup_logger
from modules.parallel import split_list, flatten
from modules.timing import timeit, elapsed_timer
logger = setup_logger(__name__, config.log_name, config.log_level)


def get_patterns_dict():
    # type: () -> dict
    """
    Returns patterns dictionary: {pattern: label}.

    :rtype: dict
    :return: patterns dictionary.
    """
    output = dict()
    for label, filename in enumerate(os.listdir(config.patterns_folder)):
        if "_" in filename:
            pattern = filename[:filename.find("_")]
            output[pattern] = label
    output[config.undetected] = -1
    return output


def get_true_labels(filenames):
    # type: (list) -> list
    """
    Returns true labels list for a given list of filenames.

    :rtype: list
    :param filenames: videos filenames.
    :return: labels list.
    """
    patterns_dict = get_patterns_dict()
    files_patterns = map(lambda name: name[:name.rfind("_")], filenames)
    return map(lambda pattern: patterns_dict[pattern], files_patterns)


def classification_report(y_true, y_pred, labels, target_names):
    # type: (list, list, list, list) -> None
    """
    Evaluates result for given true and predicted labels.

    :rtype: None
    :param y_true: true labels.
    :param y_pred: predicted labels.
    :param labels: list of labels.
    :param target_names: names matching the labels.
    :return: None.
    """
    logger.info("classification report:\n{}".format(
        metrics.classification_report(y_true, y_pred, labels=labels, target_names=target_names)))
    logger.info("confusion matrix:\n{}".format(metrics.confusion_matrix(y_true, y_pred, labels=labels)))
    logger.info("accuracy: {:.2f}%".format(metrics.accuracy_score(y_true, y_pred) * 100))


@timeit(logger, "testing patterns detector", title=True)
def test_detector(filenames, y_true):
    # type: (list, list) -> None
    """
    Tests patterns detector.

    :rtype: None
    :param filenames: videos filenames.
    :param y_true: true labels.
    :return: None.
    """

    def predict_labels(args):
        # type: (list) -> list
        """
        Predicts labels for a given list of video filenames.

        :rtype: list
        :param args: arguments: (videos filenames, process num).
        :return: predicted labels.
        """
        predicted = []
        _filenames, process_num = args
        num_videos = len(_filenames)
        for i, filename in enumerate(_filenames):
            video = cv2.VideoCapture(os.path.join(config.videos_folder, filename))
            label = detector.predict_video(video)
            predicted.append(label)
            logger.info("..process #{}: {:03d}/{:03d} videos tested".
                        format(process_num, i + 1, num_videos))
        return predicted

    patterns_dict = get_patterns_dict()
    detector = PatternsDetector(**config.detector_params)
    detector.fit(config.patterns_folder)
    filenames = split_list(filenames, config.cores)

    with elapsed_timer(logger, "process-based testing detector", title=True):
        pool = Pool(config.cores)
        pool_results = pool.imap(predict_labels, zip(filenames, range(config.cores)))
        pool.close()
        pool.join()
    y_pred = flatten(pool_results)

    labels = sorted(patterns_dict.values())
    target_names = sorted(patterns_dict, key=patterns_dict.get)
    classification_report(y_true, y_pred, labels, target_names)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    filenames = [filename for filename in os.listdir(config.videos_folder) if "_" in filename]
    y_true = get_true_labels(filenames)
    test_detector(filenames, y_true)


if __name__ == "__main__":
    main()
