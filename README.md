# @superhision

### Image Pattern Recognition in the Video Stream with OpenCV

## Tags

`object detection` `python` `opencv` `orb detector` `features matching` `flann` `adaptive histogram equalization`

## Introduction

Object detection is a computer technology related to computer vision and image processing that deals with the detection
of semantic objects instances of a particular class (such as people, buildings or cars) in digital images and videos. 
Object detection has applications in many areas of computer vision, including image retrieval and video surveillance. 
One of the methods of computer vision and image processing is a concept of feature extraction. 
It refers to methods aimed at calculating abstractions of image information and making local decisions at each pixel, 
whether or not there is an image feature of a given type at this time. 
The resulting characteristics are partial amounts of the image domain, often in the form of isolated points, continuous 
curves or connected regions. 
After features extraction, it is possible to use the method called feature matching to understand how similar two images 
are to each other.

In this project, a system that recognizes certain limited amount of image patterns in the video stream is built. 
Starting from a feature recognition method, it uses the feature matching approach to find the similarity between extracted 
features and the knowledge base features. The project is written with the aim of using it on smartphones and, therefore, 
allows to detect patterns in the video stream without requiring high computing power. The advantage of the used model
is also that it requires only one so-called training image for each pattern.

## Getting Started

These instructions allow you to reproduce the project and run it on your local machine for development and testing purposes.

### Prerequisites

The following software was used in this project:

* PyCharm: Python IDE for Professional Developers by JetBrains;
* Anaconda 4.4.0 Python 2.7 version;
* OpenCV >= 3.2 for Python;
* Python modules can be installed by `pip install`.

### Project structure

    ├── data                                    # data files
        ├── patterns                            # patterns images
        ├── renaming                            # folder supposed to keep renamed files
        ├── videos                              # testing videos
    ├── src                                     # project source
        ├── detection                           # patterns detection approach
            ├── models                          # trained models
            ├── params                          # params tuning results
            ├── ...
        ├── modules                             # additional modules of a common purpose
            ├── ...
        ├── scripts                             # auxiliary problem purposed scripts
            ├── ...
    ├── config.py
    ├── main.py
    ├── prototype.ipynb
    ├── ...    
    
`/src` has to be marked in PyCharm as sources root folder. 

### Data

Project requires knowledge base or patterns data. 
Example patterns with superheroes can be downloaded [here](https://yadi.sk/d/7FMrdKME3Nsgf4).  
They are supposed to be kept in `/data/patterns` folder with the following naming: `Pattern_.jpg`.

Since this is a smartphone-oriented project dealing with video stream data, it is tested using iPhone-made videos which 
can be downloaded [here](https://yadi.sk/d/fdcDVwef3Nsgfp).  
They are supposed to be kept in `/data/videos` folder with the following naming: `Pattern_video##.mov`.

## Approach

* Oriented FAST and rotated BRIEF (ORB) algorithm (2011).  
ORB is a fast robust local feature detector, first described in 2011, which can be used in computer vision for tasks 
such as object recognition or 3D reconstruction. It is based on the FAST Keypoint Detector and the
BRIEF Descriptor (Binary Robust Independent Elementary Features). Its goal is to offer a fast and efficient 
alternative to well-known slow SIFT algorithm.

* Fast Library for Approximate Nearest Neighbors (FLANN) Algorithm (2009).  
FLANN is a library for performing a fast approximate search for the nearest neighbors in high-dimensional spaces. 
It contains a collection of algorithms that are best suited for the nearest neighbors search, 
and a system for automatically selecting the best algorithm and optimal parameters depending on the dataset.
Naively, the matcher takes the descriptor of a feature of the first set and is matches it with all other features 
in the second set with a certain distance calculation. The closest feature or some amount of closest features are returned.  

<p align="center">
  <img src="/uploads/20130cdee0bf70c47e7d1f3e249f7e22/matching.jpg" width="600px">
</p>

Detector requires a video stream as an input. It iterates over the video frames and for each 
frame computes 2-nearest neighbors match between all pattern images and the frame. 
Regarding the comparison between the pattern with maximum number of matches and the second best pattern and due to the 
minimal required matches detector predicts the label or pattern for a given frame. 
Collecting the frame predictions detector counts them within some sliding window until it gets the vast
majority of one of the patterns at some point.

## Running the project

The main file is `main.py` located in the root folder.

It expects knowledge base or patterns data together with testing videos with the following naming: `Pattern_video##.mov`.

It is also possible to run full test from `/src/detection/test.py` which will extract true labels from the
videos names and return classification test results. 

<details> 
  <summary>Example 1</summary>
    <p align="center">
      <img src="/uploads/c935f1a3800a4e7db445a1019f9d7e98/spiderman.gif" height="600px" >
    </p>
</details>

<details> 
  <summary>Example 2</summary>
    <p align="center">
      <img src="/uploads/9d9fba66224a4bb5ea13c9f83b391d17/captainamerica.gif" height="600px" >
    </p>
</details>

<details> 
  <summary>Example 3</summary>
    <p align="center">
      <img src="/uploads/c7b32f2eb7fb2f003391914216b55fb2/batman.gif" height="600px" >
    </p>
</details>

<details> 
  <summary>Example 4</summary>
    <p align="center">
      <img src="/uploads/b8d827bd4f29447d4a8093334d5ed28f/wolverine.gif" height="600px" >
    </p>
</details>

<details> 
  <summary>Example 5</summary>
    <p align="center">
      <img src="/uploads/ad5be7bd38f9ba47bd4d068507eb4ca7/ironman.gif" height="600px" >
    </p>
</details>
