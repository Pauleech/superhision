import os
import cv2
import config
from modules.timing import elapsed_timer
from modules.logering import setup_logger
from detection.detector import PatternsDetector
logger = setup_logger(__name__, config.log_name, config.log_level)


def main():
    # type: () -> None
    """
    Runs the script.

    :rtype: None
    :return: None.
    """
    filename = os.path.join(config.videos_folder, "Spiderman_video00.mov")
    detector = PatternsDetector(**config.detector_params)
    detector.fit(config.patterns_folder)

    video = cv2.VideoCapture(os.path.join(config.videos_folder, filename))
    with elapsed_timer(logger, "testing patterns detector", title=True):
        label = detector.predict_video(video)
    pattern = detector.labels[label]
    logger.info("..{} has been detected".format(pattern))


if __name__ == "__main__":
    main()
